# CakePHP Application Skeleton

A skeleton for creating applications with [CakePHP](https://github.com/cakephp/cakephp) 2.4

## Introduction

Plugin is not checked in by default, so you have to install Plugin using composer or as a git submodule.

For example purpose I added [DebugKit](https://github.com/cakephp/debug_kit) plugin via composer and [AclExtras](https://github.com/markstory/acl_extras) as a git submodule.

CakePHP console is available from project root directory

    $ Vendor/bin/cake -app app

You will need your public key registered on GitHub before you fetch the submodule in this project.

## Getting started

Clone this project and install the project dependencies

    $ composer install
    git submodule update --init

Make sure your web server can write to `app/tmp/` directory.
